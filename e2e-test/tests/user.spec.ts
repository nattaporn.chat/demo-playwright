import { test, expect} from '@playwright/test'

test.describe("User API",() => {
    let requestContext
    const newUser =  {
        "id": Math.floor(Math.random() * 9999),
        "name": "new",
        "last_name": "new_lastname"
    }
    test.beforeAll(async ({ playwright }) => {
        requestContext = await playwright.request.newContext({
            // All requests we send go to this API endpoint.
            baseURL: 'http://localhost:3000',
            extraHTTPHeaders: {
                'Accept': 'application/json',
                'X-Correlation-Id': `Test-${Math.floor(Math.random() * 999999999999999999)}`,
            },
          })
    })
    test.beforeEach(async () => {})
    test.afterEach(async () => {})
    test.afterAll(async() => {
        // Dispose all responses.
        await requestContext.dispose();
    })

    test("Get all user",async ({ request }) => {
        const response = await requestContext.get('/users/')
        expect(response.status()).toBe(200)
        expect(response.headers()['content-type']).toBe('application/json; charset=utf-8')
        const body = await response.json()
        expect(body.data[0]).toEqual({
            "id": 1,
            "name": "Mr. John",
            "last_name": "Henry"
        })
    })

    test("Create user",async ({ request }) => {
        
        const response = await requestContext.post('/users/', {
            data : newUser
        })
        expect(response.status()).toBe(201)
        expect(response.headers()['content-type']).toBe('application/json; charset=utf-8')
    })

    test("Update exist user",async ({ request }) => {
        const updateId = newUser.id
        const updateUser =  {
            "name": "updated",
            "last_name": "new last name"
        }
        const response = await requestContext.put('/users/' + updateId, {
            data : updateUser
        })
        expect(response.status()).toBe(201)
        expect(response.headers()['content-type']).toBe('application/json; charset=utf-8')
    })

    test("Delete exist user",async ({ request }) => {
        const id = newUser.id
        const response = await requestContext.delete('/users/' + id)
        expect(response.status()).toBe(200)
        expect(response.headers()['content-type']).toBe('application/json; charset=utf-8')
    })
})