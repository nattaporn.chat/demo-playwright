import { test, expect } from '@playwright/test';

test('api hello world', async ({ request }) => {
    const response = await request.get('http://localhost:3000/')
    expect(response.status()).toBe(200)
    expect(response.headers()['content-type']).toBe('application/json; charset=utf-8')
    const body = JSON.parse(await response.text())
    expect(body).toEqual({
        "message": "Hello World!"})
})