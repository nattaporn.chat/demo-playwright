const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const app = express()
const port = 3000

// adding Helmet to enhance your API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests
app.use(cors());

// adding morgan to log HTTP requests
app.use(morgan('combined'));

let users = [{ 
  id: 1,
  name: 'Mr. John',
  last_name: 'Henry'
},{ 
  id: 2,
  name: 'Mr.Alan',
  last_name: 'Storeman'
}]
app.get('/', (req, res) => {
  res.status(200).json({ message: "Hello World!"})
})

app.get('/users', (req, res) => {
  
  res.status(200).json( { code: 200, data : users })
})

app.get('/users/:id', (req, res) => {
  
  const userId = req.params.id
  const user = findById(userId,users)
  if( user && user.length > 0) {
    return res.status(200).json({ code: 200, data : user })
  } else { 
    return res.status(404).json( {code : 404, data: null })
  }
  
})

app.post('/users/', (req, res) => {
  const newUser = req.body
  users.push (newUser)
  res.status(201).json( { code : 201 })
  
})

app.put('/users/:id', (req, res) => {
  const userId = req.params.id
  const info = req.body
  // console.log(info)
  const foundUsers = findById(userId,users)
  if(foundUsers.length > 0) {
    const user = foundUsers[0]
    user.name = info.name
    user.last_name = info.last_name
    const updateUsers = notById(userId,users)
    updateUsers.push(user)
    users = updateUsers
    console.log(users)
    res.status(201).json( { code : 201, data : users })
  }else {
    res.status(404).json( { code : 404, data : null })
  }
  
  
  
})

app.delete('/users/:id', (req, res) => {
  const userId = req.params.id
  users = notById(userId,users)
  res.status(200).json( { code : 200 })
})

function notById(id,users) {
  return users.filter(u => u.id != id)
}
function findById(id,users) {
  return users.filter(u => u.id == id)
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})